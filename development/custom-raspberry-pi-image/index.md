---
title: Custom Raspberry Pi Image
description:
icon:
weight:
author: ["steev"]
edited by : ["vatsal"]
---

The following document describes our own method of creating a **custom Kali Linux Raspberry Pi ARM image** and is targeted at developers. If you would like to install a pre-made Kali image, check out our [Install Kali on Raspberry Pi](/docs/arm/raspberry-pi/) article.

{{% notice info %}}
You'll need to have root privileges to do this procedure, or the ability to escalate your privileges with the command "sudo su".
{{% /notice %}}

### 01. Create a Kali rootfs

Build a [Kali rootfs](/docs/development/kali-linux-arm-chroot/) as described in our Kali documentation, using an **armel** architecture. By the end of this process, you should have a populated rootfs directory in **~/arm-stuff/rootfs/kali-armel**.

### 02. Create the Image File

Next, we create the physical image file, which will hold our Raspberry Pi rootfs and boot images.

```console
kali@kali:~$ sudo apt install -y kpartx xz-utils sharutils
kali@kali:~$ mkdir -p ~/arm-stuff/images/
kali@kali:~$ cd ~/arm-stuff/images/
kali@kali:~/arm-stuff/images$ dd if=/dev/zero of=kali-custom-rpi.img bs=4M count=7000
```

### 03. Partition and Mount the Image File

```console
kali@kali:~/arm-stuff/images$ parted kali-custom-rpi.img --script -- mklabel msdos
kali@kali:~/arm-stuff/images$ parted kali-custom-rpi.img --script -- mkpart primary fat32 0 64
kali@kali:~/arm-stuff/images$ parted kali-custom-rpi.img --script -- mkpart primary ext4 64 -1
```

```console
kali@kali:~/arm-stuff/images$ loopdevice=`losetup -f --show kali-custom-rpi.img`
kali@kali:~/arm-stuff/images$ device=`kpartx -va $loopdevice | sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1`
kali@kali:~/arm-stuff/images$ device="/dev/mapper/${device}"
kali@kali:~/arm-stuff/images$ bootp=${device}p1
kali@kali:~/arm-stuff/images$ rootp=${device}p2
kali@kali:~/arm-stuff/images$ mkfs.vfat $bootp
kali@kali:~/arm-stuff/images$ mkfs.ext4 $rootp
kali@kali:~/arm-stuff/images$ mkdir -p root
kali@kali:~/arm-stuff/images$ mkdir -p boot
kali@kali:~/arm-stuff/images$ mount $rootp root
kali@kali:~/arm-stuff/images$ mount $bootp boot
```

### 04. Copy and Modify the Kali rootfs

```console
kali@kali:~/arm-stuff/images$ rsync -HPavz /root/arm-stuff/rootfs/kali-armel/ root
kali@kali:~/arm-stuff/images$ echo nameserver 8.8.8.8 > root/etc/resolv.conf
```

### 05. Compile the Raspberry Pi Kernel and Modules

If you're not using ARM hardware as the development environment, you will need to set up an [ARM cross-compilation environment](/docs/development/arm-cross-compilation-environment/) to build an ARM kernel and modules. Once that's done, proceed with the following instructions.

```console
kali@kali:~/arm-stuff/images$ mkdir -p ~/arm-stuff/kernel/
kali@kali:~/arm-stuff/images$ cd ~/arm-stuff/kernel/
kali@kali:~/arm-stuff/kernel$ git clone https://github.com/raspberrypi/tools.git
kali@kali:~/arm-stuff/kernel$ git clone https://github.com/raspberrypi/linux.git raspberrypi
kali@kali:~/arm-stuff/kernel$ cd raspberrypi/
kali@kali:~/arm-stuff/kernel/raspberrypi$ touch .scmversion
kali@kali:~/arm-stuff/kernel/raspberrypi$ export ARCH=arm
kali@kali:~/arm-stuff/kernel/raspberrypi$ export CROSS_COMPILE=~/arm-stuff/kernel/toolchains/arm-eabi-linaro-4.6.2/bin/arm-eabi-
kali@kali:~/arm-stuff/kernel/raspberrypi$ make bcmrpi_cutdown_defconfig

kali@kali:~/arm-stuff/kernel/raspberrypi$ # configure your kernel !
kali@kali:~/arm-stuff/kernel/raspberrypi$ make menuconfig
kali@kali:~/arm-stuff/kernel/raspberrypi$ make -j$(cat /proc/cpuinfo|grep processor | wc -l)
kali@kali:~/arm-stuff/kernel/raspberrypi$ make modules_install INSTALL_MOD_PATH=~/arm-stuff/images/root
kali@kali:~/arm-stuff/kernel/raspberrypi$ cd ../tools/mkimage/
kali@kali:~/arm-stuff/kernel/tools/mkimage$ python imagetool-uncompressed.py ../../raspberrypi/arch/arm/boot/Image
```

```console
kali@kali:~/arm-stuff/kernel/tools/mkimage$ cd ~/arm-stuff/images/
kali@kali:~/arm-stuff/images$ git clone git://github.com/raspberrypi/firmware.git rpi-firmware
kali@kali:~/arm-stuff/images$ cp -rf rpi-firmware/boot/* boot/
kali@kali:~/arm-stuff/images$ rm -rf rpi-firmware
kali@kali:~/arm-stuff/images$ cp ~/arm-stuff/kernel/tools/mkimage/kernel.img boot/
kali@kali:~/arm-stuff/images$ echo "dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait" > boot/cmdline.txt 
```

```console
kali@kali:~/arm-stuff/images$ umount $rootp
kali@kali:~/arm-stuff/images$ umount $bootp
kali@kali:~/arm-stuff/images$ kpartx -dv $loopdevice
kali@kali:~/arm-stuff/images$ losetup -d $loopdevice
```

Use the **[dd](https://packages.debian.org/testing/dd)** command to image this file to your SD card. In our example, we assume the storage device is located at `/dev/sdb`. **Change this as needed**.

```console
kali@kali:~/arm-stuff/images$ dd if=kali-linux-rpi.img of=/dev/sdb bs=4M
```

Once the dd operation is complete, unmount and eject the SD card and boot your Pi into Kali Linux
